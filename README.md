# React Recipe App

## Introduction
This is a simple recipe application built with React and styled using Tailwind CSS. It features a main page displaying a list of recipes with images and a detail page for each recipe showing the ingredients and preparation steps. The images are clickable to open in a modal for a larger view.

## Features
- Recipe list view with names and images
- Detailed recipe view with ingredients and steps
- Clickable images that open in a modal
- Responsive design using Tailwind CSS
- JSON server for serving recipe data

## Prerequisites
Make sure you have the following installed on your machine:
- Node.js
- npm (Node Package Manager)

## Installation
1. **Clone the repository:**
   ```sh
   git clone https://gitlab.com/guckguck1/react-recipe-app.git
   ```
2. **Navigate into the project directory:**
   ```sh
   cd react-recipe-app
   ```
3. **Install the dependencies:**
   ```sh
   npm install
   ```

## Running the application
1.  **Start the JSON server:**
   ```sh
   npm run server
   ```
>This will start the JSON server at http://localhost:3001.

2. **Start the React application:**
   ```sh
   npm start
   ```
>This will start the React application at http://localhost:3000.

## Technologies Used
- React: A JavaScript library for building user interfaces
- Tailwind CSS: A utility-first CSS framework for styling
- Axios: A promise-based HTTP client for the browser and Node.js
- react-router-dom: A collection of navigational components for React
- json-server: A full fake REST API with zero coding in less than 30 seconds

## Usage
- Home Page: Displays a list of recipes with images.
- Recipe Detail Page: Shows the ingredients and steps for the selected recipe. Click on the recipe image to view it in a larger modal.
- Navigation: Use the navigation link in the header to return to the home page from the recipe detail page.

## Acknowledgements
- Recipe images are sourced from [Unsplash](https://unsplash.com/).