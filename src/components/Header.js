import React from 'react';
import { Link } from 'react-router-dom';
import { useTheme } from '../hooks/useTheme';
import ThemeSwitcherButton from './ThemeSwitcherButton';

const Header = () => {
    const { theme } = useTheme();

    return (
        <header className={`p-4 ${theme === 'light' ? 'bg-gray-800 text-white' : 'bg-gray-900 text-white'}`}>
            <nav className="flex justify-between items-center">
                <Link to="/" className="text-xl">Home</Link>
                <ThemeSwitcherButton />
            </nav>
        </header>
    );
};

export default Header;