import React from 'react';
import { useTheme } from '../hooks/useTheme';

const Footer = () => {
    const { theme } = useTheme();

    return (
        <footer className={`p-4 ${theme === 'light' ? 'bg-gray-800 text-white' : 'bg-gray-900 text-white'}`}>
            <p className="text-center">&copy; 2024 My Recipe App @Guckguck</p>
        </footer>
    );
};

export default Footer;