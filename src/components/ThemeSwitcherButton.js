import React from 'react';
import { useTheme } from '../hooks/useTheme';
import { Moon, Sun } from 'react-feather';

const ThemeSwitcherButton = () => {
    const { theme, toggleTheme } = useTheme();

    return (
        <button
            className="text-3xl ml-auto"
            onClick={toggleTheme}
        >
            {theme === 'light' ? <Moon /> : <Sun />}
        </button>
    );
};

export default ThemeSwitcherButton;
