import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const RecipeList = () => {
    const [recipes, setRecipes] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:3001/recipes')
            .then(response => setRecipes(response.data))
            .catch(error => console.error(error));
    }, []);

    return (
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
            {recipes.map(recipe => (
                <div key={recipe.id} className="border p-4 rounded-lg">
                    <Link to={`/recipes/${recipe.id}`}>
                        <img src={recipe.image} alt={recipe.name} className="w-full h-48 object-cover mb-4" />
                    </Link>
                    <h2 className="text-2xl font-bold mb-2">{recipe.name}</h2>
                </div>
            ))}
        </div>
    );
};

export default RecipeList;
