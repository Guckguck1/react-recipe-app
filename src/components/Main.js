import React from 'react';
import { useTheme } from '../hooks/useTheme';

const Main = ({ children }) => {
    const { theme } = useTheme();

    return (
        <main className={`flex-grow p-4 ${theme === 'light' ? 'bg-white text-black' : 'bg-gray-800 text-white'}`}>
            {children}
        </main>
    );
};

export default Main;
