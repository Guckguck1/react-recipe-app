import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import Modal from 'react-modal';

Modal.setAppElement('#root');

const RecipeDetail = () => {
    const { id } = useParams();
    const [recipe, setRecipe] = useState(null);
    const [isModalOpen, setIsModalOpen] = useState(false);

    useEffect(() => {
        axios.get(`http://localhost:3001/recipes/${id}`)
            .then(response => setRecipe(response.data))
            .catch(error => console.error(error));
    }, [id]);

    const openModal = () => setIsModalOpen(true);
    const closeModal = () => setIsModalOpen(false);

    if (!recipe) return <div>Loading...</div>;

    return (
        <div className="p-4">
            <h1 className="text-3xl font-bold mb-4">{recipe.name}</h1>
            <img
                src={recipe.image}
                alt={recipe.name}
                className="w-full h-64 object-cover mb-4 cursor-pointer"
                onClick={openModal}
            />
            <h2 className="text-2xl font-bold mb-2">Zutaten</h2>
            <ul className="list-disc list-inside mb-4">
                {recipe.ingredients.map((ingredient, index) => (
                    <li key={index}>{ingredient}</li>
                ))}
            </ul>
            <h2 className="text-2xl font-bold mb-2">Zubereitung</h2>
            <ol className="list-decimal list-inside">
                {recipe.steps.map((step, index) => (
                    <li key={index}>{step}</li>
                ))}
            </ol>

            <Modal
                isOpen={isModalOpen}
                onRequestClose={closeModal}
                contentLabel="Recipe Image"
                className="fixed inset-0 flex items-center justify-center"
                overlayClassName="fixed inset-0 bg-black bg-opacity-75"
            >
                <div
                    className="absolute inset-0 flex items-center justify-center"
                    onClick={closeModal}
                >
                    <div
                        className="relative bg-white p-4 rounded shadow-lg max-w-3/4 max-h-3/4"
                        onClick={(event) => event.stopPropagation()}
                    >
                        <button
                            onClick={closeModal}
                            className="absolute top-2 right-2 text-black text-3xl"
                        >
                            &times;
                        </button>
                        <img
                            src={recipe.image}
                            alt={recipe.name}
                            className="max-w-full max-h-full object-contain"
                        />
                    </div>
                </div>
            </Modal>
        </div>
    );
};

export default RecipeDetail;
