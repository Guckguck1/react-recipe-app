import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Header from './components/Header';
import Footer from './components/Footer';
import Main from './components/Main';
import RecipeList from './components/RecipeList';
import RecipeDetail from './components/RecipeDetail';
import { ThemeProvider } from './hooks/useTheme';

const App = () => (
  <Router>
    <ThemeProvider>
      <div className="flex flex-col min-h-screen">
        <Header />
        <Main>
          <Routes>
            <Route path="/" element={<RecipeList />} />
            <Route path="/recipes/:id" element={<RecipeDetail />} />
          </Routes>
        </Main>
        <Footer />
      </div>
    </ThemeProvider>
  </Router>
);

export default App;
